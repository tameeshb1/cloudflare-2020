const API_URL = "https://cfw-takehome.developers.workers.dev/api/variants";

/**
 * ElementHandler for HTMLRewriter
 * @param {action} desired action to perform on element 
 *        {replace_anchor/prepend/append}
 * @param {textContent} Text to replace_anchor/append/prepend
 */
class ElementHandler {
  constructor(action, textContent) {
    // prevents multiple edits because text is in form of a stream
    this.dirty = false
    this.action = action
    this.textContent = textContent
  }

  // Handles text inside the element
  text(text) {
    // To replace text in the anchor tag
    if (this.action == "replace_anchor" && this.dirty === false)
      text.replace(this.textContent, {
        html: true
      });
    // To prepend text
    else if (this.action == 'prepend' && this.dirty === false)
      text.before(this.textContent, {
        html: true
      })
    // to append text
    else if (text.lastInTextNode === true) {
      if (this.action == 'append')
        text.after(this.textContent, {
          html: true
        })
    }
    this.dirty = true;
  }

  // Handles the element attributes
  element(element) {
    if (this.action == "replace_anchor") {
      element.setAttribute('href', 'https://www.linkedin.com/in/tameeshb/')
      element.setAttribute('target', '_blank')
    }
  }
}

/**
 * Respond with modified HTML for A/B testing
 * @param {Request} request
 */
async function handleRequest(request) {
  let set_cookie = false;
  let variant_group;
  
  // Fetch cookies to check pre-assigned variant
  const cookie = request.headers.get('cookie')
  if (cookie && cookie.includes(`cloudflare_2020_takehome=variant_2`)) {
    variant_group = 'variant_2';
  } else if (cookie && cookie.includes(`cloudflare_2020_takehome=variant_1`)) {
    variant_group = 'variant_1';
  } else {
    // Assign a variant
    // 50/50 split among the two variants
    variant_group = Math.random() < 0.5 ? 'variant_1' : 'variant_2'
    set_cookie = true;
  }


  let APIRequestParams = {
    method: 'GET',
    headers: new Headers({}),
  }

  // Fetches the Variant URLs from given API
  variant_urls = await fetch(API_URL, APIRequestParams)
    .then(response => {
      if (response.status === 200) {
        return response.json();
      } else {
        return response.text();
        throw new Error('Something went wrong on api server!');
      }
    })
    .then(data => {
      return data.variants;
    }).catch(error => {
      console.error(error);
    });

  selected_variant_url = variant_urls[variant_group == 'variant_2' ? 1 : 0]

  RAW_HTML = await fetch(selected_variant_url, APIRequestParams)
    .then(response => {
      if (response.status === 200) {
        return response.text();
      } else {
        return response.text();
        throw new Error('Something went wrong on api server!');
      }
    })
    .then(htmlBody => {
      return htmlBody;
    }).catch(error => {
      console.error(error);
    });

  const responseHeaders = {
    headers: {
      'content-type': 'text/html;charset=UTF-8',
    },
  };
  
  // Set cookies to make variant persistent
  if (set_cookie === true)
    responseHeaders.headers['Set-Cookie'] = `cloudflare_2020_takehome=${variant_group}; path=/`;
  
  // HTML content to replace existing HTML DOM
  title_text = 'You are on a modified variant.<br>'
  body_text = `<br>Contents modified using 
  <a href="https://developers.cloudflare.com/workers/reference/apis/html-rewriter/">HTMLRewriter</a> 
  by Cloudflare.<br>
  The variant you recieve is also made to be <b>persistent</b> using cookies.`

  return new HTMLRewriter()
    .on('title', new ElementHandler('prepend', 'Your Persistent Variant : '))
    .on('h1#title', new ElementHandler('prepend', title_text))
    .on('p#description', new ElementHandler('append', body_text))
    .on('a#url', new ElementHandler('replace_anchor', 'Visit my LinkedIn'))
    .transform(new Response(RAW_HTML, responseHeaders))
}


addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
